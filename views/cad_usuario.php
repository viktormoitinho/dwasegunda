 <?php
    session_start();
    ?>
        <?php 
        if(isset($_SESSION['msg']))
            echo $_SESSION['msg'];
            unset($_SESSION['msg']);
        ?>
        
        <section class="page-section" id="contact">
            <div class="container">
              <div class="row">
                <div class="col-lg-12 text-center">
                
                <h2 class="section-heading text-uppercase"><br><br>Solicitar Orçamento</h2>
                <h3 class="section-subheading text-muted">Preencha os campos abaixo para realizar o seu pedido</h3>
                <br><br>
                </div>
              </div>
                    <form method="POST" action="views/proc_cad_usuario.php">
                        <div class="row">
                            <div class="col-lg-12">
                                <form id="contactForm" name="sentMessage" novalidate="novalidate">
                                    <div class="row"> 
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input class="form-control" type="text" name="nome" placeholder="Digite seu nome completo">
                                            </div>
                                            <div class="form-group">
                                                <input class="form-control" type="email" name="email" placeholder="Digite seu e-mail válido para entrarmos em contato">   
                                            </div>
                                            <div class="form-group">
                                                <input class="form-control" type="text" name="telefone" placeholder="Digite seu numero de telefone válido para entrarmos em contato">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <textarea class="form-control"  type="text" name="orcamento"  placeholder="Digite aqui todos os detalhes sobre o seu pedido de orçamento" ></textarea>
                                            </div>
                                        </div>
                                        
                                        <div class="col-lg-12 text-center"><br><br>
                                            <input type="submit" value="Enviar solicitação"class="btn btn-primary btn-xl text-uppercase" >
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </form>
            </div> 
        </section>                                  
  