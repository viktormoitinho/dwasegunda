<?php
session_start();
include_once("conexao.php");
$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);
$result_pedidos = "DELETE FROM pedidos WHERE id = '$id'";
$resultado_usuario = mysqli_query($conn, $result_pedidos);
if(mysqli_affected_rows($conn)){
    $_SESSION['msg'] = "<p style='color:green;'>Usuario apagado com sucesso</p>";
    header("Location: listar.php");
}else{

        $_SESSION['msg'] = "<p style='color:red;'>Usuario apagado sem sucesso</p>";
        header("Location: listar.php");
}
